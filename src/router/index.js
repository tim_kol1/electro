import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import * as Pages from '../pages/'

const AppRouter = () => {
    return(
        <Router>
            <Switch>
                {/* <Route path= '/' exact render={() => (<p>Landing</p>)} /> */}
                <Route path= '/' exact component={Pages.Landing} />
            </Switch>
        </Router>
    )
} 

export default AppRouter;